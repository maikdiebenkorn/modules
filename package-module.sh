#!/usr/bin/env bash

MODULE=$1

# Get the latest (unannotated) tag. `git describe` would return only annotated tags; abbrev=0 suppresses long format
# because it would indicate the number of commits .
LATEST_VERSION=$(git describe --tags --abbrev=0)

if [[ ! "$LATEST_VERSION" =~ ([0-9]{1}\.[0-9]{1}\.[0-9]{1}) ]]; then
    echo "no proper semVer tag present for module $MODULE!"
    echo $MODULE >> ../failed-modules
    exit
fi

# Ensure that the latest version is actually checked out.
git checkout $LATEST_VERSION

# TODO: can this be slimmed down?
mkdir -p "../build/$MODULE"
cp -pR info.json assets backend frontend locale "../build/$MODULE"

# If composer is used, install dependencies and copy them to the build dir.
if [ -f composer.json ]; then
    php $CI_PROJECT_DIR/composer.phar install;
    cp -pR vendor "../build/$MODULE"
fi

cd "../build/$MODULE"

php -r '$infoFile = json_decode(file_get_contents("info.json")); $infoFile->module->version = $argv[1]; $infoFile->module->last_update = $argv[2]; file_put_contents("info.json", json_encode($infoFile, JSON_PRETTY_PRINT));' -- "$LATEST_VERSION" "$(date +%Y-%m-%d)"

cd ..
zip -r -q "$MODULE-$LATEST_VERSION.zip" $MODULE
